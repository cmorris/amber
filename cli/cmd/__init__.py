import cmd.init
import cmd.setup
import cmd.status
import cmd.workon
import os.path
import provider

class Error(Exception):
    pass

commands = {
        'init': cmd.init,
        'status': cmd.status,
        'setup': cmd.setup,
        'workon': cmd.workon
        }

def setup(parser):
    sub = parser.add_subparsers(title='commands')
    for name, mod in commands.items():
        subp = sub.add_parser(name, help=mod.__doc__)
        subp.set_defaults(cmd_mod=mod)
        mod.setup(subp)

def run(args):
    args.cmd_mod.run(args)

def ask_yn(msg):
    '''Ask the user a yes or no question, return the answer as a boolean'''
    okay = {'y': True, 'yes': True, 'n': False, 'no': False}
    while True:
        ans = raw_input('%s (y/n): ' % msg).lower()
        if ans in okay:
            return okay[ans]
        else:
            print('Please enter (y)es or (n)o')

def ensure_setup():
    if not os.path.isfile(provider.path()):
        raise Error('Run the "setup" command first')
