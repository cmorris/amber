'''Show the modification status of the current working directory.'''
import gitwrap
git = gitwrap.git

def setup(parser):
    pass

def run(args):
    out_state = {'changed': 'M', 'added': 'A', 'deleted': 'D', 'moved': 'R'}
    for state in gitwrap.status():
        if '.amber' in state.path:
            continue

        if state.to_path:
            print('%s %s -> %s' % (out_state[state.state], state.path, state.to_path))
        else:
            print('%s %s' % (out_state[state.state], state.path))
