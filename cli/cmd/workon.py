'''Start working on a new issue, switching to or creating a new branch'''
import cmd
import gitwrap
import provider
git, gitout = gitwrap.git, gitwrap.gitout

def setup(parser):
    parser.add_argument('query', nargs='*', help='A search query to locate the ticket to work on')

def run(args):
    cmd.ensure_setup()
    if not args.query:
        ticket = create_ticket()
    else:
        ticket = find_ticket(args.query.join(' '))
    print('Working on "%s"\n - %s' % (ticket['name'], ticket['url']))
    branch = branch_name(ticket)
    current = gitwrap.current_branch()
    if current == branch:
        return

    gitwrap.stash('amstash-%s' % current)
    switch_branch(branch)
    gitwrap.unstash('amstash-%s' % branch)

def create_ticket():
    while True:
        title = raw_input('New Ticket: ').strip()
        if title != '':
            break
        else:
            print('# Please enter a ticket title')

    ticket = provider.validate_ticket(provider.call('create-ticket', {'name': title}))
    return ticket

def branch_name(ticket):
    return 'amt-%s' % ticket['id']

def switch_branch(branch):
    '''Switch to the given branch or create a new branch, and make sure it's up-to-date'''
    git('fetch')
    if branch not in gitwrap.branches():
        git('checkout', 'master')
        git('merge', 'origin/master')
        git('checkout', '-b', branch)
        git('push', '-u')
    else:
        git('checkout', branch)
        git('merge', 'origin/' + branch)
