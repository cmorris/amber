'''Set up the issue database integration for this repository'''
import cmd
import gitwrap
import os.path
import pkgutil
import provider
git = gitwrap.git

providers = {'custom': pkgutil.get_data('providers', 'custom.py')}

def setup(parser):
    parser.add_argument('provider', help='The provider to set up.', choices=list(providers.keys()))

def run(args):
    script_path = os.path.join(gitwrap.gitdir(), 'amber', 'provider')
    if os.path.exists(script_path):
        if not cmd.ask_yn('A provider was already set up for this repository. Is it okay to replace it?'):
            print('Setup cancelled')
            return

    if not os.path.isdir(os.path.dirname(script_path)):
        os.makedirs(os.path.dirname(script_path))

    open(script_path, 'w').write(providers[args.provider])
    os.chmod(script_path, 0755)
    print('Saved %s provider to %s' % (args.provider, script_path[len(gitwrap.rootdir()) + 1:]))
    provider.call('setup')
