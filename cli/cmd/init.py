'''Initialize a local checkout as an empty repository or a copy of an existing repository.'''
import cmd
import os.path
import gitwrap
git = gitwrap.git

def setup(parser):
    parser.add_argument('repo', nargs='?', help='the path or URL of a repo to copy')
    parser.add_argument('dir', nargs='?', help='the path to clone into, if not the current directory', default=os.getcwd())

def run(args):
    if os.path.exists(os.path.join(args.dir, '.git')):
        raise cmd.Error('Nothing to initialize. This directory is already a repository.')

    if not args.repo:
        git('init')
    else:
        git('clone', args.repo, args.dir)
        for branch, info in gitwrap.remote_branches('origin').items():
            if branch != 'master':
                git('branch', branch, 'origin/' + branch)
