import cmd
import gitwrap
import json
import os.path
import subprocess
import sys
import threading

class CallError(Exception):
    pass

def path():
    return os.path.join(gitwrap.gitdir(), 'amber', 'provider')

def call(command, params=None):
    if params is None:
        params = {}

    p = subprocess.Popen([path()], stdin=subprocess.PIPE, stderr=subprocess.PIPE)
    json.dump({'command': command, 'params': params}, p.stdin)
    p.stdin.write('\n')
    stdin_thread = threading.Thread(target=stdin_reader, args=(p.stdin,))
    stdin_thread.daemon = True
    stdin_thread.start()
    result = p.stderr.read()
    try:
        result = json.loads(result)
    except Exeption as e:
        result = {'error': result}

    if 'error' in result:
        raise CallError(result['error'])
    return result['success']

def stdin_reader(stdin):
    while True:
        data = sys.stdin.read(1)
        try:
            stdin.write(data)
        except:
            return

def validate_ticket(ticket):
    '''Ensure that the given ticket has the appropriate keys'''
    expected = ['id', 'name', 'url']
    for exkey in expected:
        if exkey not in ticket:
            raise CallError('The ticket looks invalid. Tickets must have a "%s" key.' % exkey)
    return ticket
