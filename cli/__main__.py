#!/usr/bin/env python
import argparse
import cmd
import sys
import traceback

parser = argparse.ArgumentParser('amber', description='Amber is an opinionated git-based version control system.')

if __name__ == '__main__':
    try:
        cmd.setup(parser)
        args = parser.parse_args()
        cmd.run(args)
    except Exception as e:
        sys.stderr.write('# ' + str(e) + '\n')
        sys.stderr.write(traceback.format_exc())
        sys.exit(1)
