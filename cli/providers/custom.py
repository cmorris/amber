#!/usr/bin/env python
import json
import random
import sys

def handle_command(cmdlist, input, output):
    request = json.loads(input.readline())
    command = cmdlist[request['command']]
    response = command(request['params'])
    output.write(json.dumps({'success': response}) + '\n')


def cmd_setup(params):
    print('This code should set any configuration params necessary')
    return True

def cmd_create_ticket(params):
    ticket = {'name': params['name']}
    print('This code should create a new ticket with the given name')
    ticket['id'] = random.randint(1, 999999)
    ticket['url'] = 'http://bugs.example.com/ticket/%s' % ticket['id']
    return ticket

commands = {'setup': cmd_setup, 'create-ticket': cmd_create_ticket}

if __name__ == '__main__':
    try:
        handle_command(commands, sys.stdin, sys.stderr)
    except Exception as e:
        sys.stderr.write(json.dumps({'error': '<%s> %s' % (type(e).__name__, str(e))}) + '\n')
