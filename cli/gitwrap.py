'''A utility module that wraps the git command line in a more programmatic way'''
import os.path
import re
import subprocess
from collections import namedtuple

FileStatus = namedtuple('FileStatus', ['path', 'state', 'to_path'])
Branch = namedtuple('Branch', ['name', 'remote', 'local'])

branchline_pat = re.compile(r'([\*| ]) ([^\s]+)[ ]+\w+ (.+)')

def git(*args):
    subprocess.check_call(['git'] + list(args))

def gitout(*args):
    return subprocess.check_output(['git'] + list(args))

def remote_branches(name):
    '''Return the list of branches available on the given remote'''
    return {k: v for k, v in branches().items() if v.remote == name}

def branches():
    '''Return all branches currently tracked'''
    ret = {}
    for line in gitout('branch', '--list', '-vv', '-a'):
        match = branchline_pat.match(line)
        if not match: continue
        remote = None
        if match.group(3).startswith('['):
            remote = match.group(3)[1:match.group(3).index(']')]
            if ':' in remote:
                remote = remote[:remote.index(':')]

        current = match.group(1) == '*'
        name = match.group(2)
        if name.startswith('remotes/'):
            name = name[len('remotes/'):]
            local = False
        else:
            local = True

        if name in ret: continue

        ret[name] = Branch(name, remote, local)

    return ret


def status():
    files = subprocess.check_output(['git', 'status', '-z']).rstrip('\0').split('\0')
    ret = []
    for path in files:
        if path == '':
            continue

        st, path = path.split(' ', 1)
        st = st[0]
        if ' -> ' in path:
            from_path, to_path = path.split(' -> ', 1)
            ret.append(FileStatus(from_path, 'moved', to_path))
        else:
            states = {'A': 'added', '?': 'added', 'M': 'changed', 'D': 'deleted'}
            if st not in states:
                continue
            ret.append(FileStatus(path, states[st], None))

    return ret

def rootdir():
    '''Return the root of the repository'''
    path = os.getcwd()
    while path != '/':
        if os.path.isdir(os.path.join(path, '.git')):
            return path
        path = os.path.dirname(path)
    return None

def gitdir():
    '''Return the .git directory for the repository'''
    return os.path.join(rootdir(), '.git')

def current_branch():
    return gitout('symbolic-ref', '--short', 'HEAD').strip()

def stash(name):
    if not status():
        return
    git('stash', 'save', '-u', 'amber %s' % name)

def unstash(name):
    for line in gitout('stash', 'list'):
        if 'amber' in line and name in line:
            git('stash', 'pop', 'stash@' + line[7])
            break
    return
